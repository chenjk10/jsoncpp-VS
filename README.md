# jsoncpp-VS
https://github.com/open-source-parsers/jsoncpp/releases 

#### 介绍
使用VS2015对jsoncpp-1.9.1编译生成32和64位静态库lib  
由于使用的cmake生成的VS工程，cmake使用的是绝对路径，所以下载的文件必须按指定路径存放，生成工程路径为：  
D:\GitProject\jsoncpp-VS\jsoncpp-1.9.1\VS2015Win32\JSONCPP.sln  
D:\GitProject\jsoncpp-VS\jsoncpp-1.9.1\VS2015x64\JSONCPP.sln  

说明博文：https://blog.csdn.net/chenjk10/article/details/101021129

生成32位库路径：  
D:\GitProject\jsoncpp-VS\jsoncpp-1.9.1\VS2015Win32\src\lib_json\Debug\jsoncpp.lib  
D:\GitProject\jsoncpp-VS\jsoncpp-1.9.1\VS2015Win32\src\lib_json\Release\jsoncpp.lib  


生成64位库路径：  
D:\GitProject\jsoncpp-VS\jsoncpp-1.9.1\VS2015x64\src\lib_json\Debug\jsoncpp.lib  
D:\GitProject\jsoncpp-VS\jsoncpp-1.9.1\VS2015x64\src\lib_json\Release\jsoncpp.lib  

头文件：
D:\GitProject\jsoncpp-VS\jsoncpp-1.9.1\include
D:\GitProject\jsoncpp-VS\jsoncpp-1.9.1\VS2015Win32\include\


##### 生成库（包含完整头文件、32和64位静态库）在如下文件夹中
**D:\GitProject\jsoncpp-VS\jsoncpp\jsoncpp-1.9.1**  

##### 直接可用源码
文件夹 **jsoncpp-VS\jsoncpp\jsoncpp-1.9.1直接源码\json** 中包含了jsoncpp的源码，已经修改了部分#include路径，可以直接将文件加到工程里，不需要连接静态库，如下图：  
![Alt text](images/20190916171704.png)
（可以只添加自己用的部分文件）
