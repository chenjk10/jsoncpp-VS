# CMake generated Testfile for 
# Source directory: D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/src/test_lib_json
# Build directory: D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/VS2015Win32/src/test_lib_json
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
if("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
  add_test(jsoncpp_test "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/VS2015Win32/src/test_lib_json/Debug/jsoncpp_test.exe")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
  add_test(jsoncpp_test "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/VS2015Win32/src/test_lib_json/Release/jsoncpp_test.exe")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
  add_test(jsoncpp_test "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/VS2015Win32/src/test_lib_json/MinSizeRel/jsoncpp_test.exe")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
  add_test(jsoncpp_test "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/VS2015Win32/src/test_lib_json/RelWithDebInfo/jsoncpp_test.exe")
else()
  add_test(jsoncpp_test NOT_AVAILABLE)
endif()
