# Install script for directory: D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/JSONCPP")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/json" TYPE FILE FILES
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/allocator.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/assertions.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/autolink.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/config.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/features.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/forwards.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/json.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/reader.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/value.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/include/json/writer.h"
    "D:/GitProject/jsoncpp-VS/jsoncpp-1.9.1/VS2015Win32/include/json/version.h"
    )
endif()

